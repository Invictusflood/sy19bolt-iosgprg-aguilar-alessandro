﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelEndCameraLock : MonoBehaviour
{
    public CameraControl CamControl;
    // Start is called before the first frame update
    void Start()
    {
        GameObject MainCam = GameObject.FindGameObjectWithTag("MainCamera");
        CamControl = MainCam.GetComponent<CameraControl>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            CamControl.TogglePlayerCameraLock();
        }
    }
}
