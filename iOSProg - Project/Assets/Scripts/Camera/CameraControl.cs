﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public Transform Target;
    public float CameraOffsetY = 0;
    public bool CameraLockedToPlayer = true;
    // Update is called once per frame
    void Update()
    {
        CameraLockToPlayer();
    }

    void CameraLockToPlayer()
    {
        if (Target != null)
        {
            if (CameraLockedToPlayer == true)
                transform.position = new Vector3(transform.position.x, Target.position.y + CameraOffsetY, transform.position.z);
        }
    }

    public void TogglePlayerCameraLock()
    {
        CameraLockedToPlayer = false;
    }
}
