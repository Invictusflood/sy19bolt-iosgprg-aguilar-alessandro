﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AmmoChanged : UnityEvent<int, int> { }

public class PlayerWeapons : MonoBehaviour
{
    public GunController EquipedGun;
    public List<GunController> GunTypes;
    public PlayerDataManager PersistentPlayerDataManager;
    public PlayerMovement PlayerMoveComponent;

    public AmmoChanged EvtAmmoChanged = new AmmoChanged();

    public float AmmoBuffMultiplier = 1;

    // Start is called before the first frame update
    void Start()
    {
        PlayerMoveComponent = gameObject.GetComponent<PlayerMovement>();
        GameObject playerDataGameObject = GameObject.FindWithTag("PlayerDataManager");
        PersistentPlayerDataManager = playerDataGameObject.GetComponent<PlayerDataManager>();

        if (PersistentPlayerDataManager != null)
        {
            SetPlayerEquippedGun();
            SetAmmoBuffMultiplier();
        }

        EvtAmmoChanged.Invoke(EquipedGun.GunAmmo.AmmoCount, EquipedGun.GunAmmo.MagazineSize);
    }

    // Update is called once per frame
    void Update()
    {
        IfGroundedReloadGun();
        ReadyGun();
    }

    void SetPlayerEquippedGun()
    {
        ChangeEquippedGun(PersistentPlayerDataManager.CurrentGunIndex);
    }

    void SetAmmoBuffMultiplier()
    {
        AmmoBuffMultiplier = PersistentPlayerDataManager.AmmoBuffMultiplier;
    }

    public void ChangeEquippedGun(int SelectedGunIndex)
    {
        EquipedGun = GunTypes[SelectedGunIndex];
        if (PersistentPlayerDataManager != null)
            PersistentPlayerDataManager.ChangeGunTypeIndex(SelectedGunIndex);
        EvtAmmoChanged.Invoke(EquipedGun.GunAmmo.AmmoCount, EquipedGun.GunAmmo.MagazineSize);
    }

    void ReadyGun()
    {
        if (!PlayerMoveComponent.IsGrounded)
            EquipedGun.ReadyWeapon();
    }

    void IfGroundedReloadGun()//Move to weapon
    {
        if (PlayerMoveComponent.IsGrounded)
        {
            ReloadGun();
        }
    }

    public void ReloadGun()
    {
        EquipedGun.Reload();
        EvtAmmoChanged.Invoke(EquipedGun.GunAmmo.AmmoCount, EquipedGun.GunAmmo.MagazineSize);
    }
}
