﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private LayerMask platformLayerMask;
    private Rigidbody2D rb2d;
    private BoxCollider2D boxCollider2D;
    private SpriteRenderer sprite;
    public PlayerDataManager PersistentPlayerDataManager;

    public float DistanceDragThreshold = 1.0f;
    public float BulletRecoilVelocity = -0.25f;
    public float StompRecoilVelocity = 10f;
    public float DamageRecoilVelocity = 20f;

    public bool UsingTouchControls = false;
    public float MovementDirection = 0;
    private float startTouchPosX, currentTouchPosX;
    private Vector2 currentVelocity; //for testing
    public bool IsGrounded;//for testing
    private bool playerMoved;

    [System.Serializable]
    public class PlayerMotionStats
    {
        public float Speed = 10f;
        public float JumpVelocity = 10f;
    }

    public PlayerMotionStats playerMovement = new PlayerMotionStats();

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        PersistentPlayerDataManager = GameObject.FindGameObjectWithTag("PlayerDataManager").GetComponent<PlayerDataManager>();
        if (PersistentPlayerDataManager != null)
            SetPlayerMovementStats();

        boxCollider2D = GetComponent<BoxCollider2D>();
        sprite = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        IsGrounded = CheckIfGrounded();
        UserInput();
    }

    void FixedUpdate()
    {
        SetVelocity();
    }

    void SetPlayerMovementStats()
    {
        rb2d.drag = PersistentPlayerDataManager.PlayerAirDrag;
        UsingTouchControls = PersistentPlayerDataManager.IsUsingTouchControls;
    }
    private void UserInput()
    {
        if (UsingTouchControls)
        {
            TouchControls();
        }
        else
        {
            PCMovement();
            PCJump();
        }
    }

    private void TouchControls()
    {
        if (Input.touchCount > 0)
        {
            currentTouchPosX = Input.GetTouch(0).position.x;
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                playerMoved = false;
                startTouchPosX = Input.GetTouch(0).position.x;
            }
            // 7/21 Consultation: You can use this to determine if a touch has moved
            //if (Input.GetTouch(0).phase == TouchPhase.Moved)
            //{
            //}
            if (Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                if (currentTouchPosX > DistanceDragThreshold + startTouchPosX)
                {
                    playerMoved = true;
                    MovementDirection = 1;
                    sprite.flipX = false;
                }
                else if (currentTouchPosX < -DistanceDragThreshold + startTouchPosX)
                {
                    playerMoved = true;
                    MovementDirection = -1;
                    sprite.flipX = true;
                }
                else
                    MovementDirection = 0;
            }

            if (IsGrounded && playerMoved == false)
            {
                if (Input.GetTouch(0).phase == TouchPhase.Ended)
                    Jump();
            }
        }
        else
            MovementDirection = 0;
    }

    private void PCMovement()
    {
        MovementDirection = Input.GetAxisRaw("Horizontal"); //Keyboard testing
        if (MovementDirection == -1)
        {
            sprite.flipX = true;
        }
        else if (MovementDirection == 1)
        {
            sprite.flipX = false;
        }
    }

    private void PCJump()
    {
        if (IsGrounded && Input.GetKeyDown(KeyCode.Space))
            Jump();
    }

    private void Jump()
    {
        rb2d.velocity = Vector2.up * playerMovement.JumpVelocity;
    }

    private void SetVelocity()
    {
        Vector2 movementVector = new Vector2(MovementDirection * playerMovement.Speed, rb2d.velocity.y);
        rb2d.velocity = movementVector;
        currentVelocity = rb2d.velocity;//for debug purposes
    }

    private bool CheckIfGrounded()
    {
        RaycastHit2D raycastHit2d = Physics2D.BoxCast(boxCollider2D.bounds.center, boxCollider2D.bounds.size, 0f, Vector2.down, .5f, platformLayerMask);
        return raycastHit2d.collider != null;
    }

    public void ApplyEnemyRecoil()
    {
        rb2d.velocity = new Vector2(rb2d.velocity.x, StompRecoilVelocity);
    }

    public void ApplyGunRecoil()
    {
        rb2d.velocity = new Vector2(rb2d.velocity.x, BulletRecoilVelocity);
    }
}
