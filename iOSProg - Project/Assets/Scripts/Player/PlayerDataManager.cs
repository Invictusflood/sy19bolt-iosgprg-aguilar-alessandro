﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
    [SerializeField]
    public int HighScore;
    [SerializeField]
    public int TotalGold;
    [SerializeField]
    public List<bool> IsCostumeUnlocked;
}

public class PlayerDataManager : MonoBehaviour
{
    public int HealthMax;
    public int Health;
    public int HealHealth = 1;
    public float AmmoBuffMultiplier = 1;
    public int PlayerAirDrag;
    public int CurrentGunIndex;
    public List<Buff> BuffBank;
    public List<Buff> ActiveBuffs;
    public PlayerData PlayerDataInstance = new PlayerData();
    public List<Costume> Costumes;
    public Costume EquippedCostume;
    public int GoldEarned;
    public bool IsUsingTouchControls = false;
    void Update()
    {
        ManualSaveLoadInput();
        UpdateHighScore();
    }

    void ManualSaveLoadInput()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            Save();
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            Load();
        }
    }

    public void TakeDamage()
    {
        Health--;
    }

    public void Heal()
    {
        Health+= HealHealth;
    }

    public void ChangeGunTypeIndex(int newGunIndex)
    {
        CurrentGunIndex = newGunIndex;
    }

    public Buff GetBuff()
    {
        Buff randomBuff = null;
        if (BuffBank.Count > 1)
            randomBuff = BuffBank[Random.Range(0, BuffBank.Count)];
        else if(BuffBank.Count == 1)
            randomBuff = BuffBank[0];
        //RemoveFromBuffBank(randomBuff);
        Debug.Log(randomBuff);
        return randomBuff;
    }

    public void AddToBuffBank(Buff selectedBuff)
    {
        BuffBank.Add(selectedBuff);
    }

    public void MoveBuffToActiveList(Buff selectedBuff)
    {
        ActiveBuffs.Add(selectedBuff);
        BuffBank.Remove(selectedBuff);
    }

    public void Load()
    {
        string json = PlayerPrefs.GetString("saveFile");
        Debug.Log("Loaded: " + json);
        PlayerData loadedData = JsonUtility.FromJson<PlayerData>(json);
        if (loadedData != null)
        {
            this.PlayerDataInstance.HighScore = loadedData.HighScore;
            this.PlayerDataInstance.TotalGold = loadedData.TotalGold;
            this.PlayerDataInstance.IsCostumeUnlocked = loadedData.IsCostumeUnlocked;
            UpdateUnlockedCostumes();
        }
        else
            Debug.Log("Error: SaveFile has not been made");
    }

    public void Save()
    {
        string json = JsonUtility.ToJson(PlayerDataInstance);
        Debug.Log(json);
        PlayerPrefs.SetString("saveFile", json);
    }

    public void UpdateUnlockedCostumes()
    {
        for(int i = 0; i < Costumes.Count; i++)
        {
            if (i < PlayerDataInstance.IsCostumeUnlocked.Count)
            {
                Costumes[i].Unlocked = PlayerDataInstance.IsCostumeUnlocked[i];
            }
        }
    }

    public void AddGold(int gold)
    {
        PlayerDataInstance.TotalGold += gold;
        GoldEarned += gold;
    }

    public void RemoveGold(int gold)
    {
        PlayerDataInstance.TotalGold -= gold;
    }

    public void EquipNewCostume(int index)
    {
        EquippedCostume = Costumes[index];
    }

    public void AttemptToPurchaseCostume(int index)
    {
        if (PlayerDataInstance.TotalGold >= Costumes[index].GoldCost)
        {
            RemoveGold(Costumes[index].GoldCost);
            Costumes[index].UnlockCostume();
            if (index < PlayerDataInstance.IsCostumeUnlocked.Count)
                PlayerDataInstance.IsCostumeUnlocked[index] = true;
        }
        else
        {
            Debug.Log("Not Enough Gold for purchase");
        }
    }

    void UpdateHighScore()
    {
        if (GoldEarned > PlayerDataInstance.HighScore)
        {
            PlayerDataInstance.HighScore = GoldEarned;
        }
    }
}
