﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollisionDetector : MonoBehaviour
{
    public PlayerMovement PlayerMoveComponent;
    public PlayerHealth PlayerHealthComponent;
    public PlayerWeapons PlayerWeaponComponent;
    // Start is called before the first frame update
    void Start()
    {
        PlayerMoveComponent = gameObject.GetComponent<PlayerMovement>();
        PlayerHealthComponent = gameObject.GetComponent<PlayerHealth>();
        PlayerWeaponComponent = gameObject.GetComponent<PlayerWeapons>();
    }
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "StompTrigger")
        {
            Debug.Log("You stomped an enemy");
            PlayerMoveComponent.ApplyEnemyRecoil();
            PlayerWeaponComponent.ReloadGun();
        }

        if (collision.tag == "GunModule")
        {
            Debug.Log("Weapon Switched");
            PlayerHealthComponent.Heal();
            PlayerWeaponComponent.ChangeEquippedGun(collision.gameObject.GetComponent<GunModule>().getGunModule());
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Enemy")
        {
            PlayerHealthComponent.TakeDamage();
            PlayerMoveComponent.ApplyEnemyRecoil();
            PlayerWeaponComponent.ReloadGun();
        }
    }
}
