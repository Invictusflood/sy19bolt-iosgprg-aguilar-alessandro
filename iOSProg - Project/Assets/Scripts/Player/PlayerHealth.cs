﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HealthChanged : UnityEvent<int, int> { }

public class PlayerHealth : MonoBehaviour
{
    [System.Serializable]
    public class PlayerStats//Currently unused
    {
        public int HealthMax = 4;
        public int Health = 4;
        public int HealHealth = 1;
    }

    public PlayerDataManager PersistentPlayerDataManager;
    public SceneLoader SceneLoaderComponent;
    public PlayerStats PlayerHealthStats = new PlayerStats();

    public HealthChanged EvtHealthChanged = new HealthChanged();

    // Start is called before the first frame update
    void Start()
    {
        PersistentPlayerDataManager = GameObject.FindWithTag("PlayerDataManager").GetComponent<PlayerDataManager>();
        SceneLoaderComponent = GameObject.FindWithTag("SceneLoader").GetComponent<SceneLoader>();
        if (PersistentPlayerDataManager != null)
        {
            SetPlayerHealthStats();
        }

        EvtHealthChanged.Invoke(PlayerHealthStats.Health, PlayerHealthStats.HealthMax);
    }

    void SetPlayerHealthStats()
    {
        PlayerHealthStats.HealthMax = PersistentPlayerDataManager.HealthMax;
        PlayerHealthStats.Health = PersistentPlayerDataManager.Health;
        PlayerHealthStats.HealHealth = PersistentPlayerDataManager.HealHealth;
    }

    public void TakeDamage()
    {
        Debug.Log("You took damage from an enemy");
        PlayerHealthStats.Health--;

        if (PersistentPlayerDataManager != null)
            PersistentPlayerDataManager.TakeDamage();

        EvtHealthChanged.Invoke(PlayerHealthStats.Health, PlayerHealthStats.HealthMax);

        if (PlayerHealthStats.Health <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        Debug.Log("Player Died");
        PersistentPlayerDataManager.Save();
        SceneLoaderComponent.LoadMainMenu();
        Destroy(gameObject);
    }

    public void Heal()
    {
        if (PlayerHealthStats.Health < PlayerHealthStats.HealthMax)
        {
            Debug.Log("You healed");
            PlayerHealthStats.Health+= PlayerHealthStats.HealHealth;

            EvtHealthChanged.Invoke(PlayerHealthStats.Health, PlayerHealthStats.HealthMax);

            if (PersistentPlayerDataManager != null)
                PersistentPlayerDataManager.Heal();
        }
        else
            Debug.Log("Already at max health");
    }
}
