﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCostume : MonoBehaviour
{
    public PlayerDataManager PersistentPlayerDataManager;
    public SpriteRenderer SpriteRendererComponent;
    // Start is called before the first frame update

    void Start()
    {
        PersistentPlayerDataManager = GameObject.FindGameObjectWithTag("PlayerDataManager").GetComponent<PlayerDataManager>();
        SpriteRendererComponent = GetComponent<SpriteRenderer>();
        SpriteRendererComponent.sprite = PersistentPlayerDataManager.EquippedCostume.SpriteAsset;
    }
}
