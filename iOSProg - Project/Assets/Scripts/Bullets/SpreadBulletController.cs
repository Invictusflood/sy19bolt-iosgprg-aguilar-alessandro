﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Consultation 7/28 - Inheritance with Unity.
public class SpreadBulletController : GunController
{
    public float BulletSpread = 45f;
    // Consultation 7/28 - Override functions for special behaviors
    public override void Shoot()
    {
        if (Time.time >= nextTimeToFire && CheckShootInput())
        {
            nextTimeToFire = Time.time + 1f / FireRate;
            SpreadShot();
            GunAmmo.AmmoCount--;
        }
    }

    void SpreadShot()
    {
        Quaternion spreadRotation = Quaternion.Euler(0, 0, -BulletSpread);
        // Spawn 3 bullets instead of 1
        Debug.Log("Spawning 3 bullets");
        PlayerMotionController.ApplyGunRecoil();
        for (int i = 0; i < 3; i++)
        {
            GameObject bullet = Instantiate(BulletPrefab, FirePoint.position, spreadRotation);
            bullet.transform.parent = gameObject.transform.parent;
            Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
            rb.AddForce(bullet.transform.up * -BulletSpeed, ForceMode2D.Impulse);
            spreadRotation *= Quaternion.Euler(0, 0, BulletSpread);
        }
    }
}
