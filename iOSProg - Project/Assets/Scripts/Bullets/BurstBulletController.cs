﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurstBulletController : GunController
{
    public float SecondsBetweenBullets = 1f;
    public int BulletsToBeShot = 3;

    public override void Shoot()
    {
        if (Time.time >= nextTimeToFire && CheckShootInput())
        {
            nextTimeToFire = Time.time + 1f / FireRate;
            StartCoroutine(BurstShot());
            GunAmmo.AmmoCount--;
        }
    }

    IEnumerator BurstShot()
    {
        //Spawn 3 bullets, while waiting a bit before firing the other
        for (int i = 0; i < BulletsToBeShot; i++)
        {
            PlayerMotionController.ApplyGunRecoil();
            GameObject bullet = Instantiate(BulletPrefab, FirePoint.position, FirePoint.rotation);
            bullet.transform.parent = gameObject.transform.parent;
            Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
            rb.AddForce(bullet.transform.up * -BulletSpeed, ForceMode2D.Impulse);
            yield return new WaitForSeconds(SecondsBetweenBullets);
        }
    }
}
