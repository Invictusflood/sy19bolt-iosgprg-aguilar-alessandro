﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour
{
    [System.Serializable]
    public class Ammo
    {
        public int MagazineSize = 6;
        public int AmmoCount;
    }

    public Ammo GunAmmo = new Ammo();
    public PlayerWeapons PlayerWeaponsComponent; 
    public PlayerMovement PlayerMotionController;
    public GameObject BulletPrefab;
    public Transform FirePoint;
    public float BulletSpeed = 10;
    public float FireRate = 15f;

    protected float nextTimeToFire = 0f;

    // Start is called before the first frame update
    void Start()
    {
        PlayerMotionController = gameObject.GetComponentInParent<PlayerMovement>();
        GunAmmo.AmmoCount = GunAmmo.MagazineSize;
        PlayerWeaponsComponent = gameObject.GetComponentInParent<PlayerWeapons>();
        ApplyAmmoBuff();
    }

    public void ReadyWeapon()
    {
        if (GunAmmo.AmmoCount > 0)
        {
            Shoot();
            PlayerWeaponsComponent.EvtAmmoChanged.Invoke(GunAmmo.AmmoCount, GunAmmo.MagazineSize);
        }
    }

    // Consultation 7/28 - Virtual functions for any behavior that can be replaced or rewritten
    public virtual void Shoot()
    {
        if (Time.time >= nextTimeToFire && CheckShootInput())
        {
            nextTimeToFire = Time.time + 1f / FireRate;
            SingleShot();
            GunAmmo.AmmoCount--;
        }
    }

    protected bool CheckShootInput()
    {
        if (PlayerMotionController.UsingTouchControls)
        {
            if (Input.touchCount > 0)
                return Input.GetTouch(0).phase == TouchPhase.Began;
            else
                return false;
        }
        else
        {
            return Input.GetKeyDown(KeyCode.Space);
        }
    }

    protected bool CheckShootInputEnded()
    {
        if (PlayerMotionController.UsingTouchControls)
        {
            if (Input.touchCount > 0)
                return Input.GetTouch(0).phase == TouchPhase.Ended;
            else 
                return false;
        }
        else
        {
            return Input.GetKeyUp(KeyCode.Space);
        }
    }

    public virtual void Reload()
    {
        if (GunAmmo.AmmoCount < GunAmmo.MagazineSize)
        {
            GunAmmo.AmmoCount = GunAmmo.MagazineSize;
            Debug.Log("Gun was reloaded");
        }
    }

    void SingleShot()
    {
        //Shoot a single bullet
        PlayerMotionController.ApplyGunRecoil();
        GameObject bullet = Instantiate(BulletPrefab, FirePoint.position, FirePoint.rotation);
        bullet.transform.parent = gameObject.transform.parent;
        Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
        rb.AddForce(bullet.transform.up * -BulletSpeed, ForceMode2D.Impulse);
    }

    public void ApplyAmmoBuff()
    {
        GunAmmo.MagazineSize = (int)(GunAmmo.MagazineSize * PlayerWeaponsComponent.AmmoBuffMultiplier);
    }
}
