﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineGunBullet : GunController
{
    public bool GunIsTriggered;
    public bool GunWasRecentlyReloaded = true;

    public override void Shoot()
    {
        ToggleGunIsTriggered();
        if (Time.time >= nextTimeToFire)
        {
            nextTimeToFire = Time.time + 1f / FireRate;
            RapidShot();
        }
    }

    public override void Reload()
    {
        if (GunAmmo.AmmoCount < GunAmmo.MagazineSize)
        {
            GunAmmo.AmmoCount = GunAmmo.MagazineSize;
            GunWasRecentlyReloaded = true;
            Debug.Log("Gun was reloaded");
        }
    }

    void ToggleGunIsTriggered()
    {
        if(GunWasRecentlyReloaded == true)
        {
            GunIsTriggered = false;
            GunWasRecentlyReloaded = false;
        }

        if (CheckShootInput())
        {
            GunIsTriggered = true;
        }

        if (CheckShootInputEnded())
        {
            GunIsTriggered = false;
        }


    }

    public void RapidShot()
    {
        //Keep Shooting when input is held
        if (GunIsTriggered)
        {
            PlayerMotionController.ApplyGunRecoil();
            GameObject bullet = Instantiate(BulletPrefab, FirePoint.position, FirePoint.rotation);
            Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
            bullet.transform.parent = gameObject.transform.parent;
            rb.AddForce(bullet.transform.up * -BulletSpeed, ForceMode2D.Impulse);
            GunAmmo.AmmoCount--;
        }
    }
}
