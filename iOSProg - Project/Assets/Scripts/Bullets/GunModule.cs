﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunModule : MonoBehaviour
{
    public int WeaponTypeIndex = 0;

    public int getGunModule()
    {
        return WeaponTypeIndex;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            Destroy(gameObject);
       }
    }
}
