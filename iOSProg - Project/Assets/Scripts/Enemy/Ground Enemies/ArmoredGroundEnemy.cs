﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmoredGroundEnemy : GroundEnemy
{
    public override void GetHit()
    {
        Debug.Log("Enemy deflected bullet");
    }
}
