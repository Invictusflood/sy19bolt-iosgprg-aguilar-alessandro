﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GroundEnemy : Enemy
{
    [SerializeField] private LayerMask platformLayerMask;
    public float RayCastDistance = 1f;
    public bool isGrounded;
    public Transform GroundEdgeDetector;
    public Transform IsGroundedDetector;
    public Transform WallDetector;

    public override void Move()
    {
        GroundMovement();
    }

    void GroundMovement()
    {
        movementVector = new Vector2(movementVector.x, rb2d.velocity.y);
        RaycastHit2D isGroundedInfo = Physics2D.Raycast(IsGroundedDetector.position, Vector2.down, RayCastDistance, platformLayerMask);
        RaycastHit2D groundEdgeInfo = Physics2D.Raycast(GroundEdgeDetector.position, Vector2.down, RayCastDistance, platformLayerMask);
        RaycastHit2D wallInfo = Physics2D.Raycast(WallDetector.position, (Vector2)Vector3.right, RayCastDistance, platformLayerMask);
        isGrounded = isGroundedInfo.collider;
        if (isGroundedInfo.collider == true)
        {
            if (groundEdgeInfo.collider == false || wallInfo.collider == true)
            {
                if (isFacingRight == true)
                {
                    movementVector = new Vector2(-Speed, rb2d.velocity.y);
                    transform.eulerAngles = new Vector3(0, -180, 0);
                    isFacingRight = false;
                }
                else
                {
                    movementVector = new Vector2(Speed, rb2d.velocity.y);
                    transform.eulerAngles = new Vector3(0, 0, 0);
                    isFacingRight = true;
                }

            }
        }
        rb2d.velocity = movementVector;
    }
}