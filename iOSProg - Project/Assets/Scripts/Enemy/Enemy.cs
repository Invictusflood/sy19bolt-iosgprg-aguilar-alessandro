﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [System.Serializable]
    public class EnemyStats
    {
        public int HealthMax = 3;
        public int Health;
    }

    public EnemyStats enemyStats = new EnemyStats();

    public PlayerDataManager PlayerDataManagerComponent;
    protected Rigidbody2D rb2d;
    public float Speed = 1;
    public int GoldReward = 5;
    protected bool isFacingRight = true;
    protected Vector2 movementVector;

    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
        movementVector = new Vector2(Speed, rb2d.velocity.y);
        enemyStats.Health = enemyStats.HealthMax;
        PlayerDataManagerComponent = GameObject.FindGameObjectWithTag("PlayerDataManager").GetComponent<PlayerDataManager>();
    }

    void FixedUpdate()
    {
        Move();
    }

    public virtual void Move()
    {

    }

    public virtual void GetHit()
    {
        Debug.Log("Enemy Hit");
        enemyStats.Health--;

        if (enemyStats.Health <= 0)
        {
            Die();
        }
    }

    protected void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Bullet")
        {
            GetHit();
        }
    }

    public void Die()
    {
        PlayerDataManagerComponent.AddGold(GoldReward);
        Destroy(gameObject);
    }
}
