﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingEnemy : Enemy
{
    protected Transform target;
    public float DistanceToPlayer = 20f;

    private void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }
    public override void Move()
    {
            FlyingMovement();
    }

    void FlyingMovement()
    {
        if (target != null)
        {
            if (Vector3.Distance(target.position, transform.position) <= DistanceToPlayer)
                rb2d.position = Vector2.MoveTowards(transform.position, target.position, Speed * Time.deltaTime);
        }
    }
}
