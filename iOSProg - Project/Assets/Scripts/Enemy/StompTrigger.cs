﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StompTrigger : MonoBehaviour
{
    public Enemy EnemyComponent;
    //Stomping an enemy is an instant kill
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            EnemyComponent.Die();
        }
    }
}
