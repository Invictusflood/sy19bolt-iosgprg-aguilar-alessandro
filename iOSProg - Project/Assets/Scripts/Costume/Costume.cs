﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Costume : MonoBehaviour
{
    public string Id;
    public bool Unlocked = false;
    public Sprite SpriteAsset;
    public int GoldCost;

    public void UnlockCostume()
    {
        Unlocked = true;
    }
}
