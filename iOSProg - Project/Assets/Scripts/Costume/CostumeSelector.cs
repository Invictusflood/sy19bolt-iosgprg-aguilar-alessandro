﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class CostumeSelector : MonoBehaviour
{
    public PlayerDataManager PlayerDataManagerComponent;
    public SceneLoader SceneLoaderComponent;
    public List<Button> CostumeButtons;
    public int SelectedIndex;
    public Image CurrentEquippedCostume;

    public GameObject SelectedText;
    TextMeshProUGUI selectedTextComponent;
    public GameObject GoldText;
    TextMeshProUGUI goldTextComponent;
    public GameObject CostText;
    TextMeshProUGUI costTextComponent;
    void Start()
    {
        PlayerDataManagerComponent = GameObject.FindWithTag("PlayerDataManager").GetComponent<PlayerDataManager>();
        SceneLoaderComponent = GameObject.FindWithTag("SceneLoader").GetComponent<SceneLoader>();
        selectedTextComponent = SelectedText.GetComponent<TextMeshProUGUI>();
        goldTextComponent = GoldText.GetComponent<TextMeshProUGUI>();
        costTextComponent = CostText.GetComponent<TextMeshProUGUI>();
        PlayerDataManagerComponent.Load();
        GetCostumeSprites();
    }

    void Update()
    {
        selectedTextComponent.text = "Selected: " + SelectedIndex;
        goldTextComponent.text = "Total Gold: " + PlayerDataManagerComponent.PlayerDataInstance.TotalGold;
    }

    void GetCostumeSprites()
    {
        for (int i = 0; i < CostumeButtons.Count; i++)
        {
            if (i < PlayerDataManagerComponent.Costumes.Count)
            {
                CostumeButtons[i].GetComponent<Image>().sprite = PlayerDataManagerComponent.Costumes[i].SpriteAsset;
            }
        }

        UpdateCurrentCostumeImage();
    }

    void UpdateCurrentCostumeImage()
    {
        CurrentEquippedCostume.sprite = PlayerDataManagerComponent.EquippedCostume.SpriteAsset;
    }

    public void SelectCostume(int index)
    {
        UpdateCostText(index);
        if (PlayerDataManagerComponent.Costumes[index].Unlocked)
        {
            EquipCostume(index);

        }
        SelectedIndex = index;
    }

    void EquipCostume(int index)
    {
        PlayerDataManagerComponent.EquipNewCostume(index);
        UpdateCurrentCostumeImage();
    }

    void UpdateCostText(int index)
    {
        if(PlayerDataManagerComponent.Costumes[index].Unlocked)
            costTextComponent.text = "Cost: Unlocked";
        else
            costTextComponent.text = "Cost: " + PlayerDataManagerComponent.Costumes[index].GoldCost.ToString();
    }

    public void BuyCostume()
    {
        if (PlayerDataManagerComponent.Costumes[SelectedIndex].Unlocked == false)
        {
            Debug.Log("Attempting to Buy..");
            PlayerDataManagerComponent.AttemptToPurchaseCostume(SelectedIndex);
            UpdateCostText(SelectedIndex);

            if(PlayerDataManagerComponent.Costumes[SelectedIndex].Unlocked)
                EquipCostume(SelectedIndex);
        }
        else
            Debug.Log("Costume Already Bought");
    }

    public void LeaveScene()
    {
        PlayerDataManagerComponent.Save();
        SceneLoaderComponent.LoadNextScene();
    }
}
