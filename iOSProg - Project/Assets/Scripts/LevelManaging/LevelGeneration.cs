﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

//Notes:
// First block set should be spawned -8 units relative to the grid's (0,0)
// Spacing between Block sets should be 18 units relative to their center (4 units apart)

public class LevelGeneration : MonoBehaviour
{
    public List<SpawnableBlock> SpawnableBlocks;
    public List<SpawnableBlock> SpawnableLevelEnds;
    public List<SpawnableBlock> SpawnAbleGunModuleBlocks;

    private SpawnableBlock lastSpawnedBlock;
    public int NumberOfBlockToSpawn = 0;
    private int lastRandNum;
    private Vector2 blockSpawnPoint = new Vector2(0,-8);
    // Start is called before the first frame update
    void Start()
    {
        GenerateLevel();
    }

    void GenerateLevel()
    {
        for (int i = 0; i < NumberOfBlockToSpawn; i++)
        {
            if (i == (NumberOfBlockToSpawn / 2) - 1)
                InstantiateGunModuleBlock();
            else
                InstantiateNewBlock();
            blockSpawnPoint.y -= 18;
        }

        InstantiateEndBlock();
    }

    void InstantiateNewBlock()
    {
        // 7/21 Consultation:  Check what blocks are comptabile with lastSpawnedBlock
        // 7/21 Consultation:  You can do this by checking lastSpawnedBlock.CompatibleLevelBlockPrefabs

        // 7/21 Consultation:  Example:
        // 7/21 Consultation:  Let's get a random block 
        int randNum = Random.Range(0, SpawnableBlocks.Count);

        while (randNum == lastRandNum)
        {
            randNum = Random.Range(0, SpawnableBlocks.Count);
        }

        SpawnableBlock nextBlockToSpawn = SpawnableBlocks[randNum];
        lastRandNum = randNum;
        lastSpawnedBlock = Instantiate(nextBlockToSpawn, blockSpawnPoint, transform.rotation);
        lastSpawnedBlock.transform.parent = gameObject.transform;
    }

    void InstantiateGunModuleBlock()
    {
        SpawnableBlock GunModuleBlock = SpawnAbleGunModuleBlocks[Random.Range(0, SpawnAbleGunModuleBlocks.Count)];
        GunModuleBlock = Instantiate(GunModuleBlock, blockSpawnPoint, transform.rotation);
        GunModuleBlock.transform.parent = gameObject.transform;
    }

    void InstantiateEndBlock()
    {
        SpawnableBlock levelEndBlock = SpawnableLevelEnds[Random.Range(0, SpawnableLevelEnds.Count)];
        levelEndBlock = Instantiate(levelEndBlock, blockSpawnPoint, transform.rotation);
        levelEndBlock.transform.parent = gameObject.transform;
    }
}
