﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameObjectSpawnPointPair
{
    public List<Transform> PossibleSpawnPoints;
    public List<GameObject> PossibleGameObjects;
}

public class GameObjectSpawner : MonoBehaviour
{
    public List<GameObjectSpawnPointPair> EnemySpawnPointPairList;

    // Start is called before the first frame update
    void Start()
    {
        SpawnGameObjects();
    }

    void SpawnGameObjects()
    {
        for (int i = 0; i < EnemySpawnPointPairList.Count; i++)
        {
            GameObject enemy = Instantiate(EnemySpawnPointPairList[i].PossibleGameObjects[Random.Range(0, EnemySpawnPointPairList[i].PossibleGameObjects.Count)],
                EnemySpawnPointPairList[i].PossibleSpawnPoints[Random.Range(0, EnemySpawnPointPairList[i].PossibleSpawnPoints.Count)].position,
                transform.rotation);
            enemy.transform.parent = gameObject.transform;
        }
    }
}
