﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadNextLevelTrigger : MonoBehaviour
{
    public SceneLoader SceneLoader;
    
    void Start()
    {
        GameObject sceneLoaderGameObject = GameObject.FindGameObjectWithTag("SceneLoader");
        SceneLoader = sceneLoaderGameObject.GetComponent<SceneLoader>();
    }
    private void OnTriggerEnter2D(Collider2D collision) //Will modify once more levels added
    {
        if (collision.tag == "Player")
        {
            //Tell sceneLoader to load a scene
            SceneLoader.LoadNextScene();
        }
    }
}
