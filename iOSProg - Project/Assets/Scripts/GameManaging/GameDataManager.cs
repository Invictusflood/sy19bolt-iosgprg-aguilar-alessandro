﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameDataManager : MonoBehaviour
{
    public bool StartedOnBuffSelection = false;
    public List<string> WorldSceneNames;
    public List<string> NonWorldSceneNames;
    public string CurrentSceneName;
    public int MaxWorlds;
    public int WorldIndex;//Each scene represents a world
    public int MaxLevelsPerWorld; //PerWorld
    public int Level;//Each world has 3 levels (reload each world thrice)
    public int NumOfLevelsToReachBuff;
    public int LevelsTillNextBuff;
    public int Score;
    // Start is called before the first frame update
    private void Start()
    {
        if (StartedOnBuffSelection)
            CurrentSceneName = NonWorldSceneNames[1];
        else
            CurrentSceneName = NonWorldSceneNames[2];
    }
    public void IncreaseLevel()
    {
        Level++;
    }
    public void IncreaseWorldIndex()
    {
        WorldIndex++;
        Level = 1;
    }

    public string GetCurrentWorldSceneName()
    {
        return WorldSceneNames[WorldIndex - 1];
    }

    public void ResetLevelsToNextBuff()
    {
        LevelsTillNextBuff = NumOfLevelsToReachBuff;
    }
}
