﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public GameObject Player;
    public TextMeshProUGUI HealthText;
    public TextMeshProUGUI AmmoText;
    public TextMeshProUGUI HighScoreText;
    public TextMeshProUGUI GoldText;

    private PlayerDataManager playerDataManagerComponent;
    private PlayerHealth playerHealth;
    private PlayerWeapons playerWeapons;

    void Awake()
    {
        playerHealth = Player.GetComponent<PlayerHealth>();
        playerWeapons = Player.GetComponent<PlayerWeapons>();
        playerHealth.EvtHealthChanged.AddListener(HealthTextUpdate);
        playerWeapons.EvtAmmoChanged.AddListener(AmmoTextUpdate);
        playerWeapons = Player.GetComponent<PlayerWeapons>();
    }

    void Start()
    {
        playerDataManagerComponent = GameObject.FindGameObjectWithTag("PlayerDataManager").GetComponent<PlayerDataManager>();
    }

    void OnDestroy()
    {
        playerHealth.EvtHealthChanged.RemoveListener(HealthTextUpdate);
        playerWeapons.EvtAmmoChanged.RemoveListener(AmmoTextUpdate);
    }

    void Update()
    {
        HighScoreText.text = "HighScore: " + playerDataManagerComponent.PlayerDataInstance.HighScore;
        GoldText.text = "Gold Earned: " + playerDataManagerComponent.GoldEarned;
    }

    void HealthTextUpdate(int value, int maxValue)
    {
        HealthText.text = ("HP: " + value.ToString() + "/" + maxValue.ToString());
    }

    void AmmoTextUpdate(int value, int maxValue)
    {
        AmmoText.text = ("AMMO: " + value.ToString() + "/" + maxValue.ToString());
    }
}
