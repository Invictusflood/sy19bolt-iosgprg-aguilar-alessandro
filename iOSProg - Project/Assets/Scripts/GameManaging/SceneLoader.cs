﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public PlayerDataManager PlayerDataManagerComponent;
    public GameDataManager GameDataManagerComponent;
    public bool LoadCostumeSelection = true;
    void Start()
    {
        if(LoadCostumeSelection)
            SceneManager.LoadSceneAsync(GameDataManagerComponent.NonWorldSceneNames[2], LoadSceneMode.Additive);
    }

    public void LoadNextScene()
    {
        PlayerDataManagerComponent.Save();
        SceneManager.UnloadSceneAsync(GameDataManagerComponent.CurrentSceneName);
        if (GameDataManagerComponent.LevelsTillNextBuff <= 0)
        {
            GameDataManagerComponent.ResetLevelsToNextBuff();
            LoadBuffSelectionScene();
        }
        else
        {   
            LoadNextLevelScene();
        }
    }

    void LoadNextLevelScene()
    {
        if (GameDataManagerComponent.Level < GameDataManagerComponent.MaxLevelsPerWorld)
        {
            GameDataManagerComponent.IncreaseLevel();
            SceneManager.LoadSceneAsync(GameDataManagerComponent.GetCurrentWorldSceneName(), LoadSceneMode.Additive);
            GameDataManagerComponent.CurrentSceneName = GameDataManagerComponent.GetCurrentWorldSceneName();
        }
        else
        {
            if (GameDataManagerComponent.WorldIndex < GameDataManagerComponent.MaxWorlds)
            {
                GameDataManagerComponent.IncreaseWorldIndex();
                SceneManager.LoadSceneAsync(GameDataManagerComponent.GetCurrentWorldSceneName(), LoadSceneMode.Additive);
                GameDataManagerComponent.CurrentSceneName = GameDataManagerComponent.GetCurrentWorldSceneName();
            }
            else
                LoadMainMenu();

        }
        GameDataManagerComponent.LevelsTillNextBuff--;
    }

    void LoadBuffSelectionScene()
    {
        GameDataManagerComponent.CurrentSceneName = GameDataManagerComponent.NonWorldSceneNames[1]; //Set currentworld to BuffSelection
        SceneManager.LoadSceneAsync(GameDataManagerComponent.NonWorldSceneNames[1], LoadSceneMode.Additive);
    }

    public void LoadMainMenu()
    {
        SceneManager.UnloadSceneAsync(GameDataManagerComponent.CurrentSceneName);
        SceneManager.UnloadSceneAsync("Persistent");
        SceneManager.LoadScene(GameDataManagerComponent.NonWorldSceneNames[0]);
    }
}
