﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[System.Serializable]
public class BuffButtonPair
{
    public Buff SelectedBuff;
    public GameObject Button;
}

public class BuffSelectionUI : MonoBehaviour
{
    public PlayerDataManager PlayerDataManagerComponent;
    public SceneLoader SceneLoaderComponent;
    public List<BuffButtonPair> BuffButtonPairs;
    void Start()
    {
        PlayerDataManagerComponent = GameObject.FindWithTag("PlayerDataManager").GetComponent<PlayerDataManager>();
        SceneLoaderComponent = GameObject.FindWithTag("SceneLoader").GetComponent<SceneLoader>();
        GetBuffs();
    }

    void GetBuffs()
    {
        for (int i = 0; i < BuffButtonPairs.Count; i++)
        {
            if (i < PlayerDataManagerComponent.BuffBank.Count)
            {
                do
                {
                    BuffButtonPairs[i].SelectedBuff = PlayerDataManagerComponent.GetBuff();
                } while (IfBuffInSelection(BuffButtonPairs[i].SelectedBuff, i));

                BuffButtonPairs[i].Button.GetComponent<Image>().sprite = BuffButtonPairs[i].SelectedBuff.BuffIcon;
                BuffButtonPairs[i].Button.transform.GetChild(0).GetComponent<Text>().text = BuffButtonPairs[i].SelectedBuff.BuffName;
            }
        }
    }

    bool IfBuffInSelection(Buff buff, int index)
    {
        bool isInSelection = false;
        for (int i = 0; i < BuffButtonPairs.Count; i++)
        {
            if (BuffButtonPairs[i].SelectedBuff == buff && i != index)
            {
                isInSelection = true;
                break;
            }
        }
        return isInSelection;
    }

    public void BuffSelected(int index)
    {
        if (BuffButtonPairs[index].SelectedBuff != null)
        {
            BuffButtonPairs[index].SelectedBuff.Activate();
            PlayerDataManagerComponent.MoveBuffToActiveList(BuffButtonPairs[index].SelectedBuff);
            LeaveScene();
        }
    }

    public void LeaveScene()
    {
        SceneLoaderComponent.LoadNextScene();
    }
}
