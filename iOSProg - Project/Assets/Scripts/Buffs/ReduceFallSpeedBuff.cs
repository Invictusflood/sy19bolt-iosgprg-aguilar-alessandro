﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReduceFallSpeedBuff : Buff
{
    public int NewPlayerAirDrag = 3;

    public override void ApplyBuff()
    {
        PlayerDataManagerComponent.PlayerAirDrag = NewPlayerAirDrag;
    }
}
