﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaxHealthBuff : Buff
{
    public int HealthIncrease = 1;

    public override void ApplyBuff()
    {
        PlayerDataManagerComponent.HealthMax += HealthIncrease;
        PlayerDataManagerComponent.Health += HealthIncrease;
    }
}
