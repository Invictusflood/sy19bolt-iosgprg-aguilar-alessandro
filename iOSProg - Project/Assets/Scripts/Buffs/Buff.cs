﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buff : MonoBehaviour
{
    public PlayerDataManager PlayerDataManagerComponent;
    public Sprite BuffIcon;
    public string BuffName;
    public void Activate()
    {
        PlayerDataManagerComponent = GameObject.FindWithTag("PlayerDataManager").GetComponent<PlayerDataManager>();
        ApplyBuff();
    }

    public virtual void ApplyBuff()
    {
    }
}
