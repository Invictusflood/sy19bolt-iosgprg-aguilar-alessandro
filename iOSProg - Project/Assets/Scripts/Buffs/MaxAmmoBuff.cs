﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaxAmmoBuff : Buff
{
    public float AmmoBuffMultiplierIncrease = .5f;

    public override void ApplyBuff()
    {
        PlayerDataManagerComponent.AmmoBuffMultiplier += AmmoBuffMultiplierIncrease;
    }
}
